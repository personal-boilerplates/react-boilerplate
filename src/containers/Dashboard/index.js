import React from "react";
import { DashboardWrapper } from "./styles";

const Dashboard = () => (
	<DashboardWrapper>
		<h1>Hello World React App</h1>
	</DashboardWrapper>
);

export default Dashboard;
