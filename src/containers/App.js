import React, { Component } from "react";
import { withRouter, Route, Switch } from "react-router-dom";
import routes from "../routes/index.js";
import Layout from "../components/Layout";
import Error from "../widgets/Error";
import Loader from "../widgets/Loader";
const App = () => {
	return (
		<React.Fragment>
			<Layout>
				<Switch>
					{routes.map(({ path, exact, component: Component, ...rest }) => (
						<Route
							key={Math.random() * 0.25}
							path={path}
							exact={exact}
							render={props => <Component {...props} {...rest} />}
						/>
					))}
				</Switch>
				<Error />
				<Loader />
			</Layout>
		</React.Fragment>
	);
};
export default withRouter(App);
