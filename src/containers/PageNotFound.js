import React from "react";
import styled from "styled-components";

const PageWrapper = styled.div`
	height: 100vh;
	display: flex;
	flex-direction: column;
	justify-content: center;
	text-align: center;
	p:first-child {
		font-size: 3rem;
		margin: 0;
		padding: 0;
	}
`;
const PageNotFound = () => (
	<PageWrapper>
		<p>Page Not Found</p>
		<p>Please contact the administrator</p>
	</PageWrapper>
);

export default PageNotFound;
