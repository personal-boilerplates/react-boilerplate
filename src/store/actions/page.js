import constant from "../../constant";
let { PAGE_TOGGLE_LOADER, PAGE_TOGGLE_NOTIF, PAGE_SET_ERROR } = constant;
export function toggleLoader(payload) {
	return { type: PAGE_TOGGLE_LOADER, payload };
}
export function toggleNotification(payload) {
	return { type: PAGE_TOGGLE_NOTIF, payload };
}
export function setError(payload) {
	return { type: PAGE_SET_ERROR, payload };
}
