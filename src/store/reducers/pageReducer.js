import constant from "../../constant";
let { PAGE_TOGGLE_LOADER, PAGE_SET_ERROR } = constant;
const initialState = {
	loadingOpen: false,
	errors: "",
	notificationOpen: true
};

export default (state = { ...initialState }, action) => {
	let { type, payload } = action;

	switch (type) {
		case PAGE_TOGGLE_LOADER:
			return {
				...state,
				loadingOpen: !!payload
			};
		case PAGE_SET_ERROR:
			return {
				...state,
				errors: payload
			};
		default:
			return state;
	}
};
