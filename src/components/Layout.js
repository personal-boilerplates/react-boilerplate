import React, { useState } from "react";
import styled, { ThemeProvider } from "styled-components";
import { theme } from "./Theme";
import { Layout } from "antd";
import GlobalStyles from "./Global";
import { ContainerFluid } from "./Container";
import { Container, Content } from "./style";

const LayoutWrapper = ({ children }) => {
	return (
		<ThemeProvider theme={theme}>
			<ContainerFluid>
				<GlobalStyles />
				<Container className="main">
					<Layout className="main-content">
						<Layout>
							<Content>{children}</Content>
						</Layout>
					</Layout>
				</Container>
			</ContainerFluid>
		</ThemeProvider>
	);
};
export default LayoutWrapper;
