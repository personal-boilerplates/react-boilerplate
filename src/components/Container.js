import styled from "styled-components";

export const Container = styled.div`
	margin-right: auto;
	margin-left: auto;
`;
export const ContainerFluid = styled(Container)`
	width: 100% !important;
	background: #f2f2f2;
	height: 100vh;
`;
export const ComponentWrapper = styled.div`
	background: none;
	.content {
		padding: 10px 50px;
	}
`;

export const FlexWrapper = styled(Container)`
	display: flex;
	justify-content: space-evenly;
`;
