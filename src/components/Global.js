import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
  body{
    overflow:inherit !important;
    a:hover{
      color:#48B1BF;
      
    }
    &::selection{
      background:#48B1BF !important;
      color:white
    }
  }
`;
export default GlobalStyles;
