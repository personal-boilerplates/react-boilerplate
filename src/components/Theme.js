export const theme = {
	primary: "",
	primaryGradient: "",
	secondary: "",
	secondaryDark: "",
	white: "",
	fontDark: "",
	fontDarkBold: "",
	errorColor: "",
	linkTextColor: "",
	phoneXS: "",
	phone: "",
	tablet: "",
	desktop: "",
	large: "",
	xtraLarge: "",
	fullHd: "",
	retina: ""
};
