import styled from "styled-components";
import { Layout } from "antd";
export const Container = styled(Layout)`
	height: 100%;
`;

export const Content = styled.div`
	flex: 1;
	@media screen and (max-width: ${props => props.theme.tablet}) {
		overflow: visible;
	}
`;
