import axios from "axios";
import { setError } from "./setError";
import constants from "../constant/";
let { TOKEN } = constants;
let axiosInstance = axios.create({
	baseURL: process.env.REACT_APP_API_URL
});

axiosInstance.interceptors.request.use(function(config) {
	let token = JSON.parse(sessionStorage.getItem(TOKEN));
	if (token) {
		config.headers.common["authorization"] = `Bearer ${token}`;
	} else {
		delete config.headers.common["authorization"];
	}
	// if (token) {
	//   let access_token = token.data.access_token
	//   axiosInstance.defaults.headers.post['Authorization'] =
	//     'Bearer ' + access_token
	//   axiosInstance.defaults.headers.patch['Authorization'] =
	//     'Bearer ' + access_token
	// }

	// if (config['X-Grant-Token']) {
	//     config.headers['X-Grant-Token'] = config['X-Grant-Token']
	// }

	// if (config['X-Grant-Type']) {
	//     config.headers['X-Grant-Type'] = config['X-Grant-Type']
	// }

	// if (config['Transaction-Type']) {
	//     config.headers['Transaction-Type'] = config['Transaction-Type']
	// }

	return config;
});

axiosInstance.interceptors.response.use(
	function(response) {
		if (response.status === 401) {
		}
		return response;
	},

	function(error) {
		let { response } = error;

		window.api_error = error;

		//
		// if(response.data instanceof Blob) {
		//     const reader = new FileReader();
		//
		//     reader.addEventListener('loadend', (e) => {
		//       const text = e.srcElement.result;
		//       response = JSON.parse(text)
		//       setError(response.message)
		//     })
		//
		//     reader.readAsText(response.data)
		//     return Promise.reject(error)
		// }
		//
		if (response) {
			if (response.config.noError) {
				// if (response) {
				//     if ( [401].includes(response.status) ){
				//         if (sessionStorage.getItem('token')){
				//             let errorMessage = 'Invalid access token (may have expired)';
				//             sessionStorage.removeItem('token')
				//             setError(errorMessage)
				//         }
				//     }
				// }
				return Promise.reject(error);
			}
			if ([401].includes(response.status)) {
				if (response.data === "Unauthorized") {
					window.location.href = "/unauthorized";
				}
				if (sessionStorage.getItem("access_token")) {
					let errorMessage = "Invalid access token (may have expired)";
					if (response && response.data) {
						errorMessage = response.data.message;
					}
					setError(errorMessage);
				} else {
					if (response.data.errors) {
						setError(response.data.errors[0].message);
					} else {
						setError(
							`Sorry, something went wrong. We're working on it and we'll get it fixed as soon as we can.`
						);
					}
				}
				sessionStorage.removeItem("login_data");
				sessionStorage.removeItem("access_token");
			} else if ([400].includes(response.status)) {
				if (response.data.error_description || response.data.message) {
					setError(response.data.error_description);
				} else {
					setError(
						`Sorry, something went wrong. We're working on it and we'll get it fixed as soon as we can.`
					);
				}
			} else if (response.data) {
				// console.log('response.data: ', response.data);
				if (response.data.errors) {
					setError(response.data.errors[0].message);
				} else {
					if (response.data.error_description) {
						setError(response.data.error_description);
					} else {
						setError(
							`Sorry, something went wrong. We're working on it and we'll get it fixed as soon as we can.`
						);
					}
				}
			}
		} else {
			// if (sessionStorage.getItem('token')) {
			if (error.message) {
				setError(`${error.name}: ${error.message}`);
			} else {
				setError(
					`Uh oh! Looks like you lost your internet connection. Please check your network settings and try again later.`
				);
			}
			// }
		}

		return Promise.reject(error);
	}
);
export default axiosInstance;
