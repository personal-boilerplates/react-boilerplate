export const setError = (message, allSession) => {
	if (typeof window.onstorage === "function") {
		window.onstorage({ key: "error", oldValue: "", newValue: message });
	}
	if (allSession) {
		localStorage.setItem("error", message);
	} else {
		sessionStorage.setItem("error", message);
	}
};
