import { connect } from "react-redux";

import Loader from "./Loader";

const mapStateToProps = ({ page }) => {
	return { ...page };
};

export default connect(mapStateToProps)(Loader);
