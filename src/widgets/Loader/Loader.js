import React from "react";
import { Modal, Spin } from "antd";
import styled from "styled-components";
const ModalWrapper = styled(Modal)`
	z-index: 9999999999;
	top: 0;
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	.ant-modal-close-x {
		display: none;
	}
	.ant-modal-body {
		width: initial;
	}
`;
class GlobalLoader extends React.Component {
	render() {
		let { loadingOpen } = this.props;
		return (
			<ModalWrapper
				wrapClassName="vertical-center-modal"
				className="loader-modal"
				visible={loadingOpen}
				footer={false}
			>
				<Spin />
			</ModalWrapper>
		);
	}
}

export default GlobalLoader;
