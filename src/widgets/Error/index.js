import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import GlobalError from "./Error";

import { setError } from "../../store/actions/page";

const mapStateToProps = ({ page }) => {
	return { ...page };
};

const mapDispatchToProps = dispatch => {
	return bindActionCreators(
		{
			closeError: () => setError(""),
			setError
		},
		dispatch
	);
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(GlobalError);
