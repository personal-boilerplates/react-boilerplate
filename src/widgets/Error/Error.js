import React from "react";
import { Modal } from "antd";

class GlobalError extends React.Component {
	constructor() {
		super();
		this.state = {
			modal: false
		};
	}

	componentDidMount() {
		window.onstorage = this.handleError;
	}

	componentDidUpdate(prevProps) {
		let { errors } = this.props;
		if (!prevProps.errors && errors) {
			let modal = Modal.warning({
				title: "An error occured",
				content: errors,
				onOk: this.closeModal
			});
			this.setState({
				modal
			});
		}
	}

	closeModal = () => {
		this.props.closeError();
		this.state.modal.destroy();
		localStorage.setItem("error", "");
		sessionStorage.setItem("error", "");
	};

	handleError = e => {
		let { setError, closeError } = this.props;

		if (e.key === "error" && e.oldValue === "") {
			setError(e.newValue);
		} else if (e.key === "error" && e.newValue === "") {
			closeError();
		}
	};

	closeError = () => {
		let { closeError, token } = this.props;

		closeError(token);
	};

	render() {
		// let { errors, closeError, token, open } = this.props

		return null;
	}
}

export default GlobalError;
