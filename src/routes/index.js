import React from "react";
import { Redirect } from "react-router-dom";
import Dashboard from "../containers/Dashboard/";
export default [
	{
		path: "/",
		exact: true,
		component: () => <Redirect to="/dashboard" />
	},
	{
		path: "/dashboard",
		exact: true,
		component: () => <Dashboard />
	}
];
